<?php

    require 'Routing.php';

    $path = trim($_SERVER['REQUEST_URI'], '/');
    $path = parse_url($path, PHP_URL_PATH);

    Routing::get('', 'DefaultController');
    Routing::get('homepage', 'PostController');
    Routing::get('my_profile', 'ProfileController');
    Routing::get('new_post', 'PostController');
    Routing::get('my_posts', 'PostController');
    Routing::post('login', 'SecurityController');
    Routing::post('register', 'SecurityController');
    Routing::get('about', 'DefaultController');
    Routing::post('logout', 'DefaultController');
    Routing::post('edit_profile', 'ProfileController');
    Routing::post('post_view', 'DefaultController');
    Routing::post('search', 'PostController');
    Routing::get('like', 'PostController');
    Routing::get('dislike', 'PostController');
    Routing::get('post_view', 'PostController');
    Routing::get('delete', 'PostController');

    Routing::run($path);