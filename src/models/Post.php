<?php

class Post
{
    private $title;
    private $description;
    private $worthSeeing;
    private $worthAvoiding;
    private $image;
    private $createdAt;
    private $like;
    private $dislike;
    private $id;

    public function __construct($title, $description, $worthSeeing, $worthAvoiding, $image, $createdAt = null, $like = 0, $dislike = 0, $id = null)
    {
        $this->title = $title;
        $this->description = $description;
        $this->worthSeeing = $worthSeeing;
        $this->worthAvoiding = $worthAvoiding;
        $this->image = $image;
        $this->createdAt = $createdAt;
        $this->like = $like;
        $this->dislike = $dislike;
        $this->id = $id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image)
    {
        $this->image = $image;
    }

    public function getWorthAvoiding()
    {
        return $this->worthAvoiding;
    }

    public function setWorthAvoiding($worthAvoiding): void
    {
        $this->worthAvoiding = $worthAvoiding;
    }

    public function getWorthSeeing()
    {
        return $this->worthSeeing;
    }

    public function setWorthSeeing($worthSeeing): void
    {
        $this->worthSeeing = $worthSeeing;
    }

    public function getLike(): int
    {
        return $this->like;
    }

    public function setLike(int $like): void
    {
        $this->like = $like;
    }

    public function getDislike(): int
    {
        return $this->dislike;
    }

    public function setDislike(int $dislike): void
    {
        $this->dislike = $dislike;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}