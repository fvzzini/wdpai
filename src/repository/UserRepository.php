<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../controllers/AppController.php';

class UserRepository extends Repository
{
    public function loginExists($login): bool
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public.users WHERE login = :login'
        );
        $stmt->bindParam(':login', $login, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false)
            return false;
        else
            return true;
    }

    public function emailExists($email): bool
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public.users WHERE email = :email'
        );
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false)
            return false;
        else
            return true;
    }

    public function isEnable(User $user): bool
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT enabled FROM public.users WHERE email = :email'
        );
        $stmt->bindParam(':email', $user->getEmail(), PDO::PARAM_STR);
        $stmt->execute();

        $enable = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($enable[0]['enabled'])
            return true;
        else
            return false;
    }

    public function addUser(User $user)
    {
        $date = new DateTime();
        $stmt = $this->database->connect()->prepare(
            'INSERT INTO public.users_details (name, surname, country, created_at, img) VALUES (?, ?, ?, ?, ?)'
        );
        $stmt->execute([
            $user->getName(),
            $user->getSurname(),
            $user->getCountry(),
            $date->format('Y-m-d'),
            $user->getImage()
        ]);

        $stmt = $this->database->connect()->prepare(
            'INSERT INTO public.users (id_users_details, email, login, password) VALUES (?, ?, ?, ?)'
        );
        $stmt->execute([
            $this->getUserDetailsId($user),
            $user->getEmail(),
            $user->getLogin(),
            $user->getPassword()
        ]);

        $stmt = $this->database->connect()->prepare(
            'INSERT INTO public.users_roles (id_users) VALUES (?)'
        );
        $stmt->execute([$this->getUserId($user)]);
    }

    public function getUser(string $loginEmail): ?User
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public.users u LEFT JOIN users_details ud ON u.id_users_details = ud.id WHERE email = :loginEmail OR login = :loginEmail'
        );
        $stmt->bindParam(':loginEmail', $loginEmail, PDO::PARAM_STR);
        $stmt->bindParam(':loginEmail', $loginEmail, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false)
        {
            return null;
        }
        return new User(
            $user['login'],
            $user['email'],
            $user['password'],
            $user['name'],
            $user['surname'],
            $user['country'],
            $user['img']
        );
    }

    public function getUserDetailsId(User $user): int
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public.users_details WHERE name = :name AND surname = :surname AND country = :country'
        );
        $stmt->bindParam(':name', $user->getName(), PDO::PARAM_STR);
        $stmt->bindParam(':surname', $user->getSurname(), PDO::PARAM_STR);
        $stmt->bindParam(':country', $user->getCountry(), PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['id'];
    }

    public function getUserId (User $user): int
    {
        $stmt = $this->database->connect()->prepare('SELECT * FROM public.users WHERE login = :login');
        $stmt->bindParam(':login', $user->getLogin(), PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['id'];
    }

    public function editUserDetails(User $user, string $name, string $surname, string $country, int $id, string $image)
    {
        $stmt = $this->database->connect()->prepare(
            'UPDATE public.users_details SET name = :name, surname = :surname, country = :country, img = :image WHERE id = :id'
        );
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':surname', $surname, PDO::PARAM_STR);
        $stmt->bindParam(':country', $country, PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->bindParam(':image', $image, PDO::PARAM_STR);
        $stmt->execute();

        $userUpdated = $this->getUser($user->getLogin());
        setcookie("login", serialize($userUpdated), time()+3600);
    }

    public function getUserStatistics($id): array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare(
            'SELECT public.users_posts(:id)'
        );
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $result[] = $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare(
            'SELECT public.users_likes(:id)'
        );
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $result[] = $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare(
            'SELECT public.users_dislikes(:id)'
        );
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $result[] = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result;
    }
}