<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Post.php';
require_once __DIR__.'/UserRepository.php';

class PostRepository extends Repository
{
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function addPost(Post $post, int $id): void
    {
        $date = new DateTime();
        $stmt = $this->database->connect()->prepare(
            'INSERT INTO public.posts (title, description, worth_seeing, worth_avoiding, added_at, id_users, image) VALUES (?, ?, ?, ?, ?, ?, ?)'
        );

        $stmt -> execute([
            $post->getTitle(),
            $post->getDescription(),
            $post->getWorthSeeing(),
            $post->getWorthAvoiding(),
            $date->format('Y-m-d'),
            $id,
            $post->getImage()
        ]);
    }

    public function getPosts(): array
    {
        $result = [];

        $stmt = $this -> database -> connect() -> prepare(
            'SELECT * FROM public.posts'
        );

        $stmt -> execute();
        $posts = $stmt -> fetchAll(PDO::FETCH_ASSOC);

        foreach ($posts as $post)
        {
            $result[] = new Post(
                $post['title'],
                $post['description'],
                $post['worth_seeing'],
                $post['worth_avoiding'],
                $post['image'],
                $post['added_at'],
                $post['like'],
                $post['dislike'],
                $post['id']
            );
        }

        return $result;
    }

    public function getUserPosts(int $id): array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public.posts WHERE id_users = :id'
        );
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $posts = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($posts as $post)
        {
            $result[] = new Post(
                $post['title'],
                $post['description'],
                $post['worth_seeing'],
                $post['worth_avoiding'],
                $post['image'],
                $post['added_at'],
                $post['like'],
                $post['dislike']
            );
        }

        return $result;
    }

    public function getPostByTitle(string $searchString): array
    {
        $searchString = '%'.strtolower($searchString).'%';

        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public.posts WHERE LOWER(title) LIKE :search OR LOWER(description) LIKE :search'
        );
        $stmt->bindParam(':search', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getPostDetails(int $id): array
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM public.posts p FULL JOIN users u ON p.id_users = u.id FULL JOIN users_details ud ON u.id_users_details = ud.id WHERE p.id = :id'
        );

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if(!$data) {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/login");
        }

        $result [] = new Post(
            $data['title'],
            $data['description'],
            $data['worth_seeing'],
            $data['worth_avoiding'],
            $data['image'],
            $data['added_at'],
            $data['like'],
            $data['dislike']
        );

        $result [] = new User(
            $data['login'],
            $data['email'],
            $data['password'],
            $data['name'],
            $data['surname'],
            $data['country'],
            $data['img']
        );

        return $result;
    }

    public function like(int $id)
    {
        $stmt = $this->database->connect()->prepare(
            'UPDATE public.posts SET "like" = "like" + 1 WHERE id = :id'
        );

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function dislike(int $id)
    {
        $stmt = $this->database->connect()->prepare(
            'UPDATE public.posts SET dislike = dislike + 1 WHERE id = :id'
        );

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function hasPermissions(int $id): bool
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT id_roles FROM public.users_roles WHERE users_roles.id_users = :id'
        );

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $roleId = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($roleId[0]['id_roles'] == 1)
            return true;
        else
            return false;
    }

    public function deletePost($id)
    {
        $stmt = $this->database->connect()->prepare(
            'DELETE FROM public.posts WHERE id = :id'
        );

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }
}