<?php

    require_once 'AppController.php';
    require_once __DIR__.'/../models/User.php';
    require_once __DIR__.'/../repository/UserRepository.php';

    class SecurityController extends AppController
    {
        private $userRepository;

        public function __construct()
        {
            parent::__construct();
            $this->userRepository = new UserRepository();
        }

        public function register()
        {
            if (!$this->isPost())
            {
                return $this->render('register');
            }

            $login = $_POST['login'];
            $password = $_POST['password'];
            $repeatedPassword = $_POST['repeat-password'];
            $email = $_POST['email'];
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $country = $_POST['country'];

            if (empty($login) || empty($password) || empty($repeatedPassword) || empty($email) || empty($name) || empty($surname) || empty($country))
            {
                return $this->render('register', ['messages' => ['Fill all fields!']]);
            }
            if (!preg_match("/^[a-zA-Z0-9]*$/", $login))
            {
                return $this->render('register', ['messages' => ['Invalid login!']]);
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                return $this->render('register', ['messages' => ['Invalid email!']]);
            }
            if ($password !== $repeatedPassword)
            {
                return $this->render('register', ['messages' => ['Passwords are not the same!']]);
            }
            if ($this->userRepository->loginExists($login) !== false)
            {
                return $this->render('register', ['messages' => ['Login is already taken!']]);
            }
            if ($this->userRepository->emailExists($email) !== false)
            {
                return $this->render('register', ['messages' => ['E-mail is already taken!']]);
            }
            else
            {
                $user = new User($login, $email, password_hash($password, PASSWORD_BCRYPT), $name, $surname, $country);
                $this->userRepository->addUser($user);

                return $this->render('register', ['messages' => ['You\'ve been successfully registered']]);
            }
        }

        public function login()
        {
            if (!$this->isPost())
            {
                return $this->render('login');
            }

            $loginEmail = $_POST['login-email'];
            $password = $_POST['password'];
            $user = $this->userRepository->getUser($loginEmail);

            if(empty($loginEmail) || empty($password))
            {
                return $this->render('login', ['messages' => ['Fill all fields!']]);
            }
            if(!$user)
            {
                return $this->render('login', ['messages' => ['User not found!']]);
            }
            if($user->getEmail() !== $loginEmail && $user->getLogin() !== $loginEmail) {
                return $this->render('login', ['messages' => ['User with this login or e-mail not exist!']]);
            }
            if(!password_verify($password, $user->getPassword())) {
                return $this->render('login', ['messages' => ['Wrong password!']]);
            }
            if(!$this->userRepository->isEnable($user))
            {
                return $this->render('login', ['messages' => ['Account is disabled!']]);
            }
            else
            {
                setcookie("login", serialize($user), time()+3600);
                $url = "http://$_SERVER[HTTP_HOST]";
                header("Location: {$url}/homepage");
            }
        }
    }