<?php

    require_once 'AppController.php';
    class DefaultController extends AppController {

        public function index() {
            $this->render('login');
        }

        public function about() {
            $this->render('about');
        }

        public function post_view() {
            $this->render('post_view');
        }

        public function logout() {
            $this->render('logout');
        }
    }