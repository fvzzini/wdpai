<?php

    require_once 'AppController.php';
    require_once __DIR__.'/../models/Post.php';
    require_once __DIR__.'/../repository/PostRepository.php';
    require_once __DIR__.'/../repository/UserRepository.php';

    class PostController extends AppController
    {

        const UPLOAD_DIRECTORY = '/../public/uploads/posts_images/';

        private $postRepository;
        private $userRepository;
        private $id;
        private $userDetailsId;

        public function __construct()
        {
            parent::__construct();
            $this -> postRepository = new PostRepository();
            $this -> userRepository = new userRepository();

            if (isset($_COOKIE['login']))
            {
                $userData = unserialize($_COOKIE['login']);
                $this -> id = $this->userRepository->getUserId($userData);
                $this -> userDetailsId = $this->userRepository->getUserDetailsId($userData);
            }
        }

        public function homepage() {
            $posts = $this -> postRepository -> getPosts();
            $this -> render('homepage', ['posts' => $posts]);
        }

        public function my_posts() {
            if (!isset($_COOKIE['login']))
            {
                return $this->render('my_posts');
            }
            $posts = $this->postRepository->getUserPosts($this->id);
            if(empty($posts))
                return $this->render('my_posts', ['messages' => ['You have not posted yet!'], 'posts' => $posts]);
            $this->render('my_posts', ['posts' => $posts]);

        }

        public function post_view() {
            if (!isset($_COOKIE['login']))
            {
                return $this->render('post_view');
            }
            $id = $_GET['id'];
            if(!$id)
                return $this->render('login');
            else {
                $permissions = $this->postRepository->hasPermissions($this->id);
                $data = $this->postRepository->getPostDetails($id);
                $this->render('post_view', ['data' => $data, 'permissions' => $permissions]);
            }
        }

        public function new_post()
        {
            if (!$this->isPost())
            {
                return $this->render('new_post');
            }

            $title = $_POST['title'];
            $description = $_POST['description'];
            $worthSeeing = $_POST['worth-seeing'];
            $worthAvoiding = $_POST['worth-avoiding'];
            $image = $_FILES['image']['name'];

            if(empty($title) || empty($description) || empty($worthSeeing) || empty($worthAvoiding) || empty($image))
            {
                return $this->render('new_post', ['messages' => ['Fill all fields!']]);
            }
            if($this->isPost() && is_uploaded_file($_FILES['image']['tmp_name']) && $this->validate($_FILES['image']))
            {
                move_uploaded_file(
                    $_FILES['image']['tmp_name'],
                    dirname(__DIR__) . self::UPLOAD_DIRECTORY . $image
                );

                $post = new Post($title, $description, $worthSeeing, $worthAvoiding, $image);
                $this->postRepository->addPost($post, $this->id);

                return $this->render('new_post', ['messages' => ['Post added successfully!']]);
            }
        }

        public function search()
        {
            $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

            if($contentType === "application/json")
            {
                $content = trim(file_get_contents("php://input"));
                $decoded = json_decode($content, true);

                header('Content-Type: application/json');
                http_response_code(200);

                echo json_encode($this->postRepository->getPostByTitle($decoded['search']));
            }
        }

        public function like(int $id)
        {
            $this->postRepository->like($id);
            http_response_code(200);
        }

        public function dislike(int $id)
        {
            $this->postRepository->dislike($id);
            http_response_code(200);
        }

        public function delete(int $id)
        {
            $this->postRepository->deletePost($id);
            http_response_code(200);
        }
    }