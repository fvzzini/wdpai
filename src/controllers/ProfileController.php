<?php

    require_once __DIR__.'/../models/User.php';
    require_once __DIR__.'/../repository/UserRepository.php';

    class ProfileController extends AppController
    {
        const UPLOAD_DIRECTORY = '/../public/uploads/profile_images/';
        private $userRepository;
        private $userDetailsId;

        public function __construct()
        {
            parent::__construct();
            $this->userRepository = new UserRepository();

            if (isset($_COOKIE['login']))
            {
                $userData = unserialize($_COOKIE['login']);
                $this -> userDetailsId = $this->userRepository->getUserDetailsId($userData);
            }
        }

        public function my_profile()
        {
            if(!isset($_COOKIE['login']))
            {
                return $this -> render('my_profile');
            }
            $user = $this -> userRepository -> getUser(unserialize($_COOKIE['login'])->getLogin());
            $userStatistics = $this ->userRepository->getUserStatistics($this->userRepository->getUserId(unserialize($_COOKIE['login'])));
            $this -> render('my_profile', ['user' => $user, 'userStatistics' => $userStatistics]);
        }

        public function edit_profile()
        {

            if (!$this->isPost())
            {
                return $this->render('edit_profile');
            }

            $user = unserialize($_COOKIE['login']);
            $name = empty($_POST['name']) ? $user->getName() : $_POST['name'];
            $surname = empty($_POST['surname']) ? $user->getSurname() : $_POST['surname'];
            $country = empty($_POST['country']) ? $user->getCountry() : $_POST['country'];
            $image = $_FILES['image']['name'];

            if($this->isPost() && is_uploaded_file($_FILES['image']['tmp_name']) && $this->validate($_FILES['image']))
            {
                move_uploaded_file(
                    $_FILES['image']['tmp_name'],
                    dirname(__DIR__).self::UPLOAD_DIRECTORY.$image
                );
                $this->userRepository->editUserDetails($user, $name, $surname, $country, $this->userDetailsId, $image);
                return $this->render('edit_profile', ['messages' => ['Profile updated successfully!']]);
            }
            $this->userRepository->editUserDetails($user, $name, $surname, $country, $this->userDetailsId, $user->getImage());
            return $this->render('edit_profile', ['messages' => ['Profile updated successfully!']]);
        }
    }