<?php
    if(!isset($_COOKIE["login"]))
    {
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/login");
    }
?>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/1604fb9e17.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="/public/css/navbar.css">
    <link rel="icon" href="/public/img/icon.svg" type="image/svg" sizes="16x16">
</head>
<body>
<div class="base-container">
    <nav class="navbar">
        <ul class="navbar-nav">
            <li class="nav-arrow">
                <a class="nav-link">
                    <span class="link-text">menu</span>
                    <i class="fas fa-angle-double-right"></i>
                </a>
            </li>
            <li class="nav-item">
                <a href="homepage" class="nav-link">
                    <i class="fas fa-home"></i>
                    <span class="link-text">homepage</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="my_profile" class="nav-link">
                    <i class="fas fa-address-card"></i>
                    <span class="link-text">my profile</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="new_post" class="nav-link">
                    <i class="fas fa-plus"></i>
                    <span class="link-text">add new post</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="my_posts" class="nav-link">
                    <i class="fas fa-sticky-note"></i>
                    <span class="link-text">my posts</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="fas fa-user-friends"></i>
                    <span class="link-text">my friends</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="about" class="nav-link">
                    <i class="fas fa-question"></i>
                    <span class="link-text">about us</span>
                </a>
            </li>
        </ul>
    </nav>
    <main>
        <header>
            <div class="logo">
                <a href="homepage">
                    <img src="/public/img/logo.svg" alt="error_loading_img">
                </a>
            </div>
            <div class="button">
                <a href="new_post" class="button-link">
                    <i class="fas fa-plus"></i>
                    <span class="button-text">add post</span>
                </a>
            </div>
            <div class="button">
                <a href="my_profile" class="button-link">
                    <i class="fas fa-address-card"></i>
                    <span class="button-text">my profile</span>
                </a>
            </div>
            <div class="button">
                <a href="logout" class="button-link">
                    <i class="fas fa-sign-out-alt"></i>
                    <span class="button-text">sign out</span>
                </a>
            </div>
        </header>