<?php include_once 'header.php' ?>
<!DOCTYPE html>
<head>
    <script type="text/javascript" src="/public/js/statistics.js" defer></script>
    <script type="text/javascript" src="/public/js/delete.js" defer></script>
    <link rel="stylesheet" type="text/css" href="/public/css/post_view.css">
    <title>traveloop - <?= $data[0]->getTitle(); ?></title>
    <?php include_once 'header.php'; ?>
    <section>
        <div>
            <div class="header">
                <h1><?= $data[0]->getTitle(); ?></h1>
                <div class="line">
                    <hr>
                </div>
            </div>
            <div id="post-container">
                <div id="post-details">
                    <div id="img"><img src="/public/uploads/posts_images/<?= $data[0]->getImage(); ?>"></div>
                    <div class="label">Overall trip description</div>
                    <div class="label-content"><?= $data[0]->getDescription(); ?></div>
                    <div class="label">Worth seeing</div>
                    <div class="label-content"><?= $data[0]->getWorthSeeing(); ?></div>
                    <div class="label">Worth avoiding</div>
                    <div class="label-content"><?= $data[0]->getWorthAvoiding(); ?></div>
                </div>
                <div id="post-statistics">
                    <div id="left">
                        <div id="user-img"><img src="/public/uploads/profile_images/<?= $data[1]->getImage(); ?>"></div>
                            <div id="info">
                                <div><b>Added <?= $data[0]->getCreatedAt(); ?> by:</b></div>
                                <div id="nickname"><?= $data[1]->getLogin(); ?></div>
                                <div id="email"><?= $data[1]->getEmail(); ?></div>
                            </div>
                    </div>
                    <div id="right">
                        <div><i class="fas fa-heart"></i><span id="likes"><?= $data[0]->getLike(); ?></span></div>
                        <div><i class="fas fa-thumbs-down"></i><span id="dislikes"><?= $data[0]->getDislike(); ?></span></div>
                        <?php
                            if($permissions)
                            {
                                echo '<div><i class="fas fa-trash-alt"></i></div>';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
<?php include_once 'footer.php'?>
