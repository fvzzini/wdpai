<!DOCTYPE html>
<head>
    <title>traveloop - my posts</title>
    <link rel="stylesheet" type="text/css" href="/public/css/posts.css">
        <?php include_once 'header.php' ?>
            <div id="header">
                <h1>My posts</h1>
            </div>
            <div class="line">
                <hr>
            </div>
            <div class="message">
                <?php if(isset($messages))
                {
                    foreach($messages as $message)
                        echo $message;
                }
                ?>
            </div>
            <section class="posts">
                <?php foreach ($posts as $post): ?>
                    <div id="<?= $post -> getId(); ?>" style="min-width: 0;" onclick="location.href='post_view?id=<?= $post -> getId(); ?>';">
                        <img src="public/uploads/posts_images/<?= $post -> getImage(); ?>" alt="error_loading_img">
                        <div>
                            <h2><?= $post -> getTitle(); ?></h2>
                            <p><?= $post -> getDescription(); ?></p>
                            <div class="social-section">
                                <div><i class="fas fa-heart"></i><span id="likes"><?= $post -> getLike(); ?></span></div>
                                <div><i class="fas fa-thumbs-down"></i><span id="dislikes"><?= $post -> getDislike(); ?></span></div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
<?php include_once 'footer.php' ?>