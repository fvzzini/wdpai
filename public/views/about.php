<!DOCTYPE html>
<head>
    <title>traveloop - about us</title>
    <link rel="stylesheet" type="text/css" href="/public/css/about.css">
        <?php include_once 'header.php' ?>
            <section>
                <div>
                    <div class="header">
                        <h1>About us</h1>
                        <div class="line">
                            <hr>
                        </div>
                    </div>
                    <div id="about">
                        <div>
                            <p>traveloop is a web app created to bring togerher travel fans from around the world</p>
                            <i class="fas fa-infinity"></i>
                            <p>
                                the purpose of this app is to allow users to share impressions,
                                recommend places of interest and warn others against dangers or
                                uninteresting locations
                            </p>
                            <i class="fas fa-infinity"></i>
                            <p class="contact" style="padding-top: 2rem;"><b>e-mail: </b>traveloop.support@traveloop.com</p>
                            <p class="contact"><b>phone: </b>+48 000 000 000</p>
                        </div>
                    </div>
                </div>
<?php include_once 'footer.php' ?>