<!DOCTYPE html>
<head>
    <title>traveloop - homepage</title>
    <script type="text/javascript" src="/public/js/search.js" defer></script>
    <link rel="stylesheet" type="text/css" href="/public/css/posts.css">
        <?php include_once 'header.php' ?>
            <div class="search-bar">
                <input class="search" placeholder="search posts">
                <button class="search-button" type="submit">search</button>
            </div>
            <div class="line">
                <hr>
            </div>
            <section class="posts">
                <?php foreach ($posts as $post): ?>
                    <div id="<?= $post -> getId(); ?>" style="min-width: 0;" onclick="location.href='post_view?id=<?= $post -> getId(); ?>';">
                        <img src="public/uploads/posts_images/<?= $post -> getImage(); ?>" alt="error_loading_img">
                        <div>
                            <h2><?= $post -> getTitle(); ?></h2>
                            <p><?= $post -> getDescription(); ?></p>
                            <div class="social-section">
                                <div><i class="fas fa-heart"></i><span id="likes"><?= $post -> getLike(); ?></span></div>
                                <div><i class="fas fa-thumbs-down"></i><span id="dislikes"><?= $post -> getDislike(); ?></span></div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
<?php include_once 'footer.php' ?>

                <template id="post-template">
                    <div id="">
                        <img src="" alt="error_loading_img">
                        <div>
                            <h2>title</h2>
                            <p>description</p>
                            <div class="social-section">
                                <div><i class="fas fa-heart"></i><span id="likes">0</span></div>
                                <div><i class="fas fa-thumbs-down"></i><span id="dislikes">0</span></div>
                            </div>
                        </div>
                    </div>
                </template>