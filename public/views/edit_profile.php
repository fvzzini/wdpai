<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/public/css/new_post_edit_profile.css">
    <title>traveloop - edit profile</title>
    <?php include_once 'header.php' ?>
        <section>
            <div>
                <div class="header">
                    <h1>Edit profile</h1>
                    <div class="line">
                        <hr>
                    </div>
                </div>
                <div id="inputs">
                    <form id="add-new-post" action="edit_profile" method="POST" ENCTYPE="multipart/form-data">
                        <div class="message">
                            <?php if(isset($messages))
                            {
                                foreach($messages as $message)
                                    echo $message;
                            }
                            ?>
                        </div>
                        <label for="name"><b>Name:</b></label><br>
                        <input type="text" name="name" placeholder="<?= unserialize($_COOKIE["login"])->getName(); ?>"><br>
                        <label for="surname"><b>Surname:</b></label><br>
                        <input type="text" name="surname" placeholder="<?= unserialize($_COOKIE["login"])->getSurname(); ?>"><br>
                        <label for="country"><b>Country:</b></label><br>
                        <input type="text" name="country" placeholder="<?= unserialize($_COOKIE["login"])->getCountry(); ?>"><br>
                        <label for="image"><b>Select your profile image:</b></label><br>
                        <input type="file" name="image"><br>
                        <input type="submit" id="button" value="save changes">
                    </form>
                </div>
            </div>
<?php include_once 'footer.php' ?>