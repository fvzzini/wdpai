<!DOCTYPE html>
<head>
    <title>traveloop - add new post</title>
    <link rel="stylesheet" type="text/css" href="/public/css/new_post_edit_profile.css">
        <?php include_once 'header.php' ?>
            <section>
                <div>
                    <div class="header">
                        <h1>Add new post</h1>
                            <div class="line">
                                <hr>
                            </div>
                    </div>
                    <div id="inputs">
                        <form id="add-new-post" action="new_post" method="POST" ENCTYPE="multipart/form-data">
                            <div class="message">
                                <?php if(isset($messages))
                                {
                                    foreach($messages as $message)
                                        echo $message;
                                }
                                ?>
                            </div>
                            <label for="title"><b>Title:</b></label><br>
                            <input type="text" name="title" placeholder="title"><br>
                            <label for="image"><b>Select main post image:</b></label><br>
                            <input type="file" name="image"><br>
                            <label for="description"><b>Overall trip description:</b></label><br>
                            <textarea name="description"></textarea><br>
                            <label for="worth-seeing"><b>Worth seeing:</b></label><br>
                            <textarea name="worth-seeing"></textarea><br>
                            <label for="worth-avoiding"><b>Worth avoiding:</b></label><br>
                            <textarea name="worth-avoiding"></textarea><br>
                            <input type="submit" id="button" value="add post">
                        </form>
                    </div>
                </div>
<?php include_once 'footer.php' ?>