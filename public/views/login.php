<?php
    require_once 'logged.php';
?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/public/css/login_register.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">
    <link rel="icon" href="/public/img/icon.svg" type="image/svg" sizes="16x16">
    <title>traveloop</title>
</head>
<body>
    <div class="container">
        <div class="logo-login">
            <img src="/public/img/logo.svg" alt="error_loading_img">
        </div>
        <div class="login-container">
            <form class="login-form" action="login" method="POST">
                <div class="message">
                    <?php if(isset($messages))
                    {
                        foreach($messages as $message)
                            echo $message;
                    }
                    ?>
                </div>
                <div class="input-1">
                    <input name="login-email" type="text" placeholder="login / e-mail">
                </div>
                <div class="input-2">
                    <input name="password" type="password" placeholder="password">
                </div>
                <div class="buttons">
                    <span>
                        <button type="button" id="create-acc-button" onclick="location.href='register'">register now</button>
                    </span>
                    <span>
                        <button type="submit" id="login-button">login</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</body>