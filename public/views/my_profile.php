<!DOCTYPE html>
<head>
    <title>traveloop - my profile</title>
    <link rel="stylesheet" type="text/css" href="/public/css/my_profile.css">
        <?php include_once 'header.php' ?>
            <section>
                <div id="profile-1">
                    <div class="header">
                        <h1>Your profile</h1>
                            <div class="line">
                                <hr>
                            </div>
                        <div class="edit-profile">
                            <a href="edit_profile" id="edit-button">
                                <i class="far fa-edit"></i>
                                <span>edit profile</span>
                            </a>
                        </div>
                    </div>
                    <div id="profile-photo-container">
                        <div class="profile-photo">
                            <img src="public/uploads/profile_images/<?= $user -> getImage(); ?>" alt="error_loading_img">
                        </div>
                        <p id="nickname"><?= $user -> getLogin(); ?></p>
                    </div>
                </div>
                <div id="profile-2">
                    <table id="user-data">
                        <tr>
                            <td>name:</td>
                            <td class="details"><?= $user -> getName(); ?></td>
                        </tr>
                        <tr>
                            <td>surname:</td>
                            <td class="details"><?= $user -> getSurname(); ?></td>
                        </tr>
                        <tr>
                            <td>e-mail:</td>
                            <td class="details"><?= $user -> getEmail(); ?></td>
                        </tr>
                        <tr>
                            <td>country:</td>
                            <td class="details"><?= $user -> getCountry(); ?></td>
                        </tr>
                    </table>
                    <div class="line">
                        <hr>
                    </div>
                    <table id="user-stats">
                        <tr>
                            <td><i class="fas fa-sticky-note"></i></td>
                            <td class="left-td">your posts:</td>
                            <td class="right-td"><? echo $userStatistics[0]["users_posts"]; ?></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-heart"></i></td>
                            <td class="left-td">your likes:</td>
                            <td class="right-td"><? echo $userStatistics[1]["users_likes"]; ?></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-thumbs-down"></i></td>
                            <td class="left-td">your dislikes:</td>
                            <td class="right-td"><? echo $userStatistics[2]["users_dislikes"]; ?></td>
                        </tr>
                    </table>
                </div>
<?php include_once 'footer.php' ?>