<?php
    require_once 'logged.php';
?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/public/css/login_register.css">
    <script type="text/javascript" src="/public/js/script.js" defer></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">
    <link rel="icon" href="/public/img/icon.svg" type="image/svg" sizes="16x16">
    <title>traveloop</title>
</head>
<body>
    <div class="container" style="justify-content: flex-start;">
        <div class="logo-login" style="margin: 1rem;">
            <img src="/public/img/logo.svg" alt="error_loading_img">
        </div>
        <div class="login-container">
            <form class="login-form" action="register" method="POST">
                <div class="message">
                    <?php if(isset($messages))
                    {
                        foreach($messages as $message)
                            echo $message;
                    }
                    ?>
                </div>
                <div class="input-1">
                    <input name="login" type="text" placeholder="login">
                </div>
                <div class="input-2-flat">
                    <input name="password" type="password" placeholder="password">
                </div>
                <div class="input-1">
                    <input name="repeat-password" type="password" placeholder="repeat password">
                </div>
                <div class="input-2-flat">
                    <input name="email" type="text" placeholder="e-mail">
                </div>
                <div class="input-1">
                    <input name="name" type="text" placeholder="name">
                </div>
                <div class="input-2-flat">
                    <input name="surname" type="text" placeholder="surname">
                </div>
                <div class="input-2-register">
                    <input name="country" type="text" placeholder="country">
                </div>
                <div class="buttons">
                    <span>
                        <button type="button" id="create-acc-button" onclick="location.href='login'">sign in</button>
                    </span>
                    <span>
                        <button type="submit" id="login-button">create account</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</body>