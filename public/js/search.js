const search = document.querySelector('input[placeholder="search posts"]');
const button = document.querySelector(".search-button");
const postContainer = document.querySelector(".posts");


search.addEventListener("keyup", function (event) {
    if(event.key === "Enter") {
        event.preventDefault();

        const data = {search: this.value};
        if(!data)
            return false;

        fetch("/search", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response){
            return response.json();
        }).then(function (posts) {
            postContainer.innerHTML = "";
            loadPosts(posts)
        });
    }
});

button.addEventListener("click", function () {

    const data = {search: search.value};

    fetch("/search", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(function (response){
        return response.json();
    }).then(function (posts) {
        postContainer.innerHTML = "";
        loadPosts(posts)
    });
});

function loadPosts(posts) {
    posts.forEach(posts => {
        console.log(posts);
        createPost(posts);
    });
}

function createPost(post) {
    const template = document.querySelector("#post-template");

    const clone = template.content.cloneNode(true);

    const image = clone.querySelector("img");
    image.src = `public/uploads/posts_images/${post.image}`;
    const title = clone.querySelector("h2");
    title.innerHTML = post.title;
    const description = clone.querySelector("p");
    description.innerHTML = post.description;
    const likeLabel = clone.querySelector(".fa-heart");
    likeLabel.innerHTML;
    const dislikeLabel = clone.querySelector(".fa-thumbs-down");
    dislikeLabel.innerHTML;
    const like = clone.querySelector("#likes");
    like.innerText = post.like;
    const dislike = clone.querySelector("#dislikes");
    dislike.innerText = post.dislike;

    postContainer.appendChild(clone);
}