const deleteButton = document.querySelectorAll(".fa-trash-alt");

function deletePost()
{
    const id = window.location.href.split('=')[1];

    fetch(`/delete/${id}`)
        .then(function () {
            //alert("Post deleted successfully!");
            let url = window.location.href;
            url = url.split('/')[0];
            location.replace(url.concat('homepage'));
        })
}

deleteButton.forEach(button => button.addEventListener("click", deletePost));