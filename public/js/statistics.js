const likeButtons = document.querySelectorAll(".fa-heart");
const dislikeButtons = document.querySelectorAll(".fa-thumbs-down");


function giveLike() {
    const likes = document.getElementById("likes");
    const container = document.getElementById("post-container");
    const id = window.location.href.split('=')[1];

    fetch(`/like/${id}`)
        .then(function () {
            likes.innerHTML = parseInt(likes.innerHTML) + 1;
        })
}

function giveDislike() {
    const dislikes = document.getElementById("dislikes");
    const container = document.getElementById("post-container");
    const id = window.location.href.split('=')[1];

    fetch(`/dislike/${id}`)
        .then(function () {
            dislikes.innerHTML = parseInt(dislikes.innerHTML) + 1;
        })
}

likeButtons.forEach(button => button.addEventListener("click", giveLike));
dislikeButtons.forEach(button => button.addEventListener("click", giveDislike));